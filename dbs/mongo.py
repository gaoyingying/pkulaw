# -*- coding: utf-8 -*-

import asyncio
import pymongo

setting = {
    "replset": "rs0",
    "nodes": ["127.0.0.1:27017",
              "127.0.0.1:27018",
              "127.0.0.1:27019"],
    "dbname": "mercury",
    "username": "mercury",
    "password": "mercury123",
}

async def _connect_mongdb():

    client = pymongo.MongoClient(
        host=setting["nodes"],
        replicaSet=setting.get("replset"),
        connect=False,
        username=setting.get("username"),
        password=setting.get("password"),
        authSource=setting.get("dbname")
    )
    mgdb = client.get_database(
        setting['dbname'],
        read_preference=pymongo.ReadPreference.SECONDARY_PREFERRED)
    return mgdb

loop = asyncio.get_event_loop()
mgdb = loop.run_until_complete(_connect_mongdb())

