# -*- coding: utf-8 -*-


import random
from time import sleep
import requests

login_url = "http://www.pkulaw.com/login?menu=&isAutoLogin=false"

class BaseSpider:

    SHOULD_STOP = False

    def __init__(self, **kwargs):
        self.account = kwargs.pop("account", None)
        self.passwd = kwargs.pop("passwd", '')
        self.url = kwargs.pop("url", None)
        self.data_cnt = kwargs.pop("data_cnt", 10)
        self.session = requests.Session()
        self.page = self.login_bz338()

    def get_headers(self):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
            'Cookie': 'DSFirstAccess=1534356467; DSLastAccess=1534356491; DSLaunchURL=2F2C44616E61496E666F3D686D2E62616964752E636F6D2C53534C2B686D2E6769663F63633D3026636B3D3126636C3D32342D6269742664733D313434307839303026766C3D3738312665703D37343438373134253243333034332665743D3326666C3D32392E30266A613D30266C6E3D7A682D636E266C6F3D30266C743D3135333433333435393826726E643D3236343930343631362673693D38323636393638363632633038366633346232613365326165393031346266382673753D687474702533412532462532467777772E706B756C61772E636E25324626763D312E322E3334266C763D3326736E3D3239313738; DSSignInURL=/; DSID=be30fef301743440252b5782864d5142; DSCK:/:DSPT:Hm_lvt_8266968662c086f34b2a3e2ae9014bf8=1534334598%2C1534342063%2C1534348494%2C1534353013%3B%20%20domain%3Dpkulaw.com%3B%20%20path%3D/%3B%20%20expires%3DThu%2C%2015%20Aug%202019%2017%3A10%3A29%20GMT%3B%20; DSCK:/:DSPT:Hm_lpvt_8266968662c086f34b2a3e2ae9014bf8=1534353030%3B%20%20domain%3Dpkulaw.com%3B%20%20path%3D/%3B%20; DSCK:/:DSPT:gr_user_id=b3b37b8f-ca82-48ad-93cf-7cc11b033132%3B%20%20expires%3DSat%2C%2012%20Aug%202028%2017%3A10%3A29%20GMT%3B%20%20domain%3D.pkulaw.com%3B%20%20path%3D/%3B%20; DSCK:gr_session_id_81eec0d0f0be2df1=26e8830a-b915-44e7-b31d-b8d6a418ae03%3B%20%20expires%3DWed%2C%2015%20Aug%202018%2017%3A40%3A29%20GMT%3B%20%20domain%3D.pkulaw.com%3B%20%20path%3D/%3B%20; DSCK:gr_session_id_81eec0d0f0be2df1_26e8830a-b915-44e7-b31d-b8d6a418ae03=false%3B%20%20expires%3DWed%2C%2015%20Aug%202018%2017%3A40%3A29%20GMT%3B%20%20domain%3D.pkulaw.com%3B%20%20path%3D/%3B%20'
        }
        return headers

    def login_bz338(self):
        response = self.session.get(self.url, headers=self.get_headers(), verify=False)
        print("account status_code:", response.status_code)
        return response.text

    # 得到总记录数并计算总的页数
    def get_page_cnt(self, page):
       return 0

    # 构造 POST 请求
    def make_request(self, page_num=0):
        return "page"

    # 取出该账户下最后查询的关键字
    def get_last_data(self, account):
        # 测试点1：账户之前爬取过，数据库的最后一条数据是否正确
        # 测试点2：账户为曾爬取，取最后一条会不会报错，不存在时最后一条返回: NoneType
        last_inst = self.Model.get_last(account)
        return last_inst

    # 解析检索关键字并存库
    def parse(self, page, last_data=None):
        pass

    # 单个账户登录
    def start_by_common(self):
        page_cnt = self.get_page_cnt(self.page)
        last_data = self.get_last_data(self.account)
        if page_cnt:
            # 从第0页开始构造请求
            for page_num in range(0, page_cnt + 200):
                if True == self.SHOULD_STOP:
                    print("project stop")
                    return

                sleep(random.randint(10, 12))
                page = self.make_request(page_num=page_num)
                self.parse(page, last_data)


if __name__ == "__main__":


    '''
        readme：
        1，需要修改账户名密码：
              vpn 登录时，不用账户名密码登陆的接口，但是需要给定账户名，数据库里需要存放 account 字段 
        2，修改请求的页面范围：
              range(1784, page_cnt + 200): 根据需求自定制，记录上一次运行爬去的页数，断网可以继续从上一次断开的页数开始。
              +200含义： 每个账号第一次运行时，一边爬一边不断地有新的检索数据生成，在一开始统计的页数上面加上往后数一部分，确保能爬到最后一页
    '''


