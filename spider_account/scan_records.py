# -*- coding: utf-8 -*-


import os
import sys
sys.path.insert(0, os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))))

from datetime import datetime
from lxml import etree

from models.scan_records import ScanRecords
from spider_account._base import BaseSpider


class ScanSpider(BaseSpider):

    Model = ScanRecords

    # 构造 POST 请求
    def make_request(self, page_num=0):
        data = {
            'Pager.PageIndex': page_num,
            'Pager.PageSize': self.data_cnt,
            'FilterItems.Menu':'',
            'isEng': 'False',
            'X-Requested-With': 'XMLHttpRequest'
        }
        response = self.session.post(self.url, data=data, headers=self.get_headers())
        print("request page_num:", page_num)
        return response.text

    def get_page_cnt(self, page):
        # 浏览记录只显示100条，需要在页面上测试有多少页
        return 5000

    # 解析浏览记录
    def parse(self, page, last_data=None):
        try:
            html = etree.HTML(page)
            record_list = html.xpath("//div[@class='table']/table/tbody/tr")
            if not record_list:
                self.SHOULD_STOP = True
                print("no data, stop")
                return

            for info in record_list:
                scan_records = info.xpath("./td[position()=1]//a/text()")[0]
                scan_records_url = info.xpath("./td[position()=1]//a")[0].get("href")
                scan_records_url = "https://vpn.uibe.edu.cn" + scan_records_url
                time_str = info.xpath("./td[position()=2]//text()")[0]
                time = datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S")
                lawdb = info.xpath("./td[position()=3]/text()")[0]
                data = {
                    "account": self.account,
                    "getway": "pkulaw",
                    "scan_records": scan_records,
                    "scan_records_url": scan_records_url,
                    "lawdb": lawdb,
                    "time": time
                }

                if last_data and time == last_data.time:
                    print("Same as the record in the database:data is {0}, time is {1}：", data, time)
                    self.SHOULD_STOP = True
                    return
                inst = ScanRecords(**data)
                ScanRecords.save(inst)
        except Exception as e:
            print("parse_error:", e)


if __name__ == "__main__":

    record_url = "http://www.pkulaw.com/usercenter/mybrowse/False"
    data = {
        'account': 'hankun',
        'passwd': 'hk6789',
        'url': record_url,
        'data_cnt': 20
    }

    scan = ScanSpider(**data)
    scan.start_by_common()
