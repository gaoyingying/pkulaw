# -*- coding: utf-8 -*-


import random
from time import sleep
import requests

login_url = "http://www.pkulaw.com/login?menu=&isAutoLogin=false"

class BaseSpider:

    SHOULD_STOP = False

    def __init__(self, **kwargs):
        self.account = kwargs.pop("account", None)
        self.passwd = kwargs.pop("passwd", '')
        self.url = kwargs.pop("url", None)
        self.data_cnt = kwargs.pop("data_cnt", 10)
        self.session = requests.Session()
        self.page = self.login(self.account, self.passwd)

    def get_headers(self):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
        }
        return headers

    def login(self, account, password):
        data = {
            'LoginName': account,
            'LoginPwd': password
        }
        response = self.session.post(login_url, data=data, headers=self.get_headers())
        print("login status:",response.text)
        # TODO:登陆不成功也返回状态200,页面不跳转，200起不到判断作用
        if response.status_code == 200:
            # 登陆成功的页面：即数据的第一页
            sleep(5)
            response = self.session.get(self.url, headers=self.get_headers())
            print("account status_code:", response.status_code)
            return response.text


    def login_vpn(self):
        response = self.session.get(self.url, headers=self.get_headers())
        print("account status_code:", response.status_code)
        return response.text

    # 得到总记录数并计算总的页数
    def get_page_cnt(self, page):
       return 0

    # 构造 POST 请求
    def make_request(self, page_num=0):
        return "page"

    # 取出该账户下最后查询的关键字
    def get_last_data(self, account):
        # 测试点1：账户之前爬取过，数据库的最后一条数据是否正确
        # 测试点2：账户为曾爬取，取最后一条会不会报错，不存在时最后一条返回: NoneType
        last_inst = self.Model.get_last(account)
        return last_inst

    # 解析检索关键字并存库
    def parse(self, page, last_data=None):
        pass

    # 单个账户登录
    def start_by_common(self):
        page_cnt = self.get_page_cnt(self.page)
        last_data = self.get_last_data(self.account)
        if page_cnt:
            # 从第0页开始构造请求
            for page_num in range(page_cnt + 200):
                if True == self.SHOULD_STOP:
                    print("project stop")
                    return

                sleep(random.randint(10, 12))
                page = self.make_request(page_num=page_num)
                self.parse(page, last_data)


if __name__ == "__main__":


    '''
        readme：
        1，需要修改账户名密码：
              vpn 登录时，不用账户名密码登陆的接口，但是需要给定账户名，数据库里需要存放 account 字段 
        2，修改请求的页面范围：
              range(1784, page_cnt + 200): 根据需求自定制，记录上一次运行爬去的页数，断网可以继续从上一次断开的页数开始。
              +200含义： 每个账号第一次运行时，一边爬一边不断地有新的检索数据生成，在一开始统计的页数上面加上往后数一部分，确保能爬到最后一页
    '''


