# -*- coding: utf-8 -*-


import os
import sys
sys.path.insert(0, os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))))

from datetime import datetime
from lxml import etree

from models.search_keywords import SearchKeywords
from spider_account._base import BaseSpider


class SearchSpider(BaseSpider):

    Model = SearchKeywords

    def get_page_cnt(self, page):
        # 第一次采集需要知道最后一页，根据页面的数据量来计算
        # 每页的数据量计算一共有多少页, 向上取整, 页面是从0开始的
        try:
            html = etree.HTML(page)
            keyword_cnt = html.xpath("//div[@class='tool-bar']//strong/text()")[0]
            page_cnt = int(keyword_cnt) // self.data_cnt
            print("recorde count:", keyword_cnt, "page_cnt:", page_cnt)
        except Exception as e:
            print("get_page_cnt error:", e)
        else:
            return page_cnt

    # 构造 POST 请求
    def make_request(self, page_num=0):
        data = {
            'Pager.PageIndex': page_num,
            'Pager.PageSize': self.data_cnt,
            'FilterItems.Menu': '',
            'recordId': '',
            'searchType': 'keyword',
            'isEng': 'False',
            'X-Requested-With': 'XMLHttpRequest'
        }
        response = self.session.post(self.url, data=data, headers=self.get_headers())
        print("request page_num:", page_num)
        return response.text

    # 解析浏览记录
    def parse(self, page, last_data=None):
        try:
            html = etree.HTML(page)
            record_list = html.xpath("//div[@class='table']//tr[position()>1]")
            if not record_list:
                self.SHOULD_STOP = True
                print("no data, stop")
                return

            for info in record_list:
                keyword = info.xpath("./td[position()=1]//a/text()")[0]
                lawdb = info.xpath("./td[position()=2]/text()")[0]
                time_str = info.xpath("./td[position()=3]//text()")[0]
                time = datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S")
                data = {
                    "account": self.account,
                    "getway": "pkulaw",
                    "keyword": keyword,
                    "lawdb": lawdb,
                    "time": time
                }

                # 增量爬取设置: 对时间作出比较，如果解析到数据库中的最后一个数据，停止解析
                if last_data and time == last_data.time:
                    print("Same as the record in the database:data is {0}, time is {1}：", data, time)
                    # 检测到已经爬去到上一次的位置，停止
                    self.SHOULD_STOP = True
                    return
                inst = SearchKeywords(**data)
                SearchKeywords.save(inst)
        except Exception as e:
            print("parse_error:", e)


if __name__ == "__main__":

    keywords_url = "http://www.pkulaw.com/usercenter/mysearchkeyword/False"
    data = {
        'account': 'hankun',
        'passwd': 'hk6789',
        'url': keywords_url,
        'data_cnt': 20
    }

    searchkeywords = SearchSpider(**data)
    searchkeywords.start_by_common()
