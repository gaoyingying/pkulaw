# -*- coding: utf-8 -*-

import os


# ####################
# Basic paths
# ####################
DIR_SETTING = os.path.dirname(os.path.abspath(__file__))
DIR_PROJECT = os.path.dirname(os.path.dirname(DIR_SETTING))