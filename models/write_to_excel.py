# -*- coding: utf-8 -*-

import json

import xlwt


def read_data():

    with open("search_word.txt", "r", encoding="utf-8") as ff:
        data_list = []
        for line in ff.readlines():
            line = line.strip()
            try:
                data = json.loads(line)
            except:
                continue
            data_list.append(data)
        return data_list


def write_to_excel(data_list):
    PATH = "检索记录.xls"
    book1 = xlwt.Workbook()

    sheet1 = book1.add_sheet("检索记录")

    sheet1.write(0, 0, "账户")
    sheet1.write(0, 1, "来源")
    sheet1.write(0, 2, "关键词")
    sheet1.write(0, 3, "数据库")
    sheet1.write(0, 4, "时间")


    for info in data_list:
        sheet1.write(data_list.index(info) + 1, 0, info["account"])
        sheet1.write(data_list.index(info) + 1, 1, info["getway"])
        sheet1.write(data_list.index(info) + 1, 2, info["keyword"])
        sheet1.write(data_list.index(info) + 1, 3, info["lawdb"])
        sheet1.write(data_list.index(info) + 1, 4, info["time"])
        book1.save(PATH)

    # PATH = "浏览记录.xls"
    # book2 = xlwt.Workbook()
    # sheet2 = book2.add_sheet("浏览记录")
    #
    # sheet2.write(0, 0, "账户")
    # sheet2.write(0, 1, "来源")
    # sheet2.write(0, 2, "浏览记录")
    # sheet2.write(0, 3, "浏览链接")
    # sheet2.write(0, 4, "数据库")
    # sheet2.write(0, 5, "查询时间")
    #
    # for info in data_list:
    #     sheet2.write(data_list.index(info)+1, 0, info["account"])
    #     sheet2.write(data_list.index(info)+1, 1, info["getway"])
    #     sheet2.write(data_list.index(info)+1, 2, info["scan_records"])
    #     sheet2.write(data_list.index(info)+1, 3, info["scan_records_url"])
    #     sheet2.write(data_list.index(info)+1, 4, info["lawdb"])
    #     sheet2.write(data_list.index(info)+1, 5, info["time"])
    #     book2.save(PATH)


if __name__=="__main__":

    data_list = read_data()
    print(data_list)
    write_to_excel(data_list)

