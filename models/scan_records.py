# -*- coding: utf-8 -*-


from dbs.mongo import mgdb
from models._base import Model


class ScanRecords(Model):

    _db = mgdb.scan_word

    def __init__(self, **kwargs):
        self._id = kwargs.pop("_id", None)
        self.account = kwargs.pop("account", None)              # 账号
        self.getway = kwargs.pop("getway", None)                # 来源
        self.scan_records = kwargs.pop("scan_records", None)    # 浏览记录
        self.scan_records_url = kwargs.pop("scan_records_url", None)
        self.lawdb = kwargs.pop("lawdb", None)                  # 所属数据库
        self.time = kwargs.pop("time", None)                    # 查询时间


if __name__ == '__main__':

    ScanRecords.get_random(count=520)