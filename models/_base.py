# -*- coding: utf-8 -*-
from datetime import datetime

from bson import ObjectId
from pymongo.errors import DuplicateKeyError


class Model:

    def to_mongo(self):
        """返回实例在 MongoDB 中的存储数据结构
        """
        if self._id is None:
            del self._id
        elif isinstance(self._id, str):
            self._id = ObjectId(self._id)
        data = self.__dict__.copy()
        return data

    def save(self):
        data = self.to_mongo()
        try:
            result = self._db.insert_one(data)
            self._id = result.inserted_id
        except DuplicateKeyError as e:
            print("[DB]<{0}> already exists".format(data))
        except Exception as e:
            print("[DB] inserted: error: {0}".format(data))
        else:
            print("[DB] inserted: succeed: {0}".format(data))

    # 根据账户名称找出该账户下的最近一次记录
    @classmethod
    def get_last(cls, account):
        last_data = cls._db.find({"account": account}).sort([("time", -1)]).limit(1)
        for cursor in last_data:
            inst = cls(**cursor)
            print("[DB] finding: {0}".format(inst.__dict__))
            return inst
        else:
            print("[DB] there is no record of the account here：{0} ".format(account))
            return None

    # 从数据库随机返回500条，近一年的数据
    @classmethod
    def get_random(cls, count=500):
        items = cls._db.aggregate([
            {"$match":
                {"$and":
                    [
                        {"time":{"$gt":"2017-10-29 0:0:0"}},
                        {"time":{"$lt":"2018-10-29 0:0:0"}}
                    ]
                }
            },
            {"$sample": {'size':count}}
        ])

        for cursor in items:
            inst = cls(**cursor)
            del inst._id
            inst.time = datetime.strptime(inst.time, '%Y-%m-%dT%H:%M:%S.000Z')
            inst.time = datetime.strftime(inst.time, "%Y-%m-%d  %H:%M:%S")
            with open("search_word.txt", "w+", encoding="utf-8") as ff:
                ff.write("{}\n".format(inst.__dict__))

        else:
            return None



