# -*- coding: utf-8 -*-

from dbs.mongo import mgdb
from models._base import Model


class SearchKeywords(Model):

    _db = mgdb.search_word

    def __init__(self, **kwargs):
        self._id = kwargs.pop("_id", None)
        self.account = kwargs.pop("account", None)   # 账号
        self.getway = kwargs.pop("getway", None)     # 来源
        self.keyword = kwargs.pop("keyword", None)   # 检索关键字
        self.lawdb = kwargs.pop("lawdb", None)       # 所属数据库
        self.time = kwargs.pop("time", None)         # 查询时间


if __name__ == '__main__':

    SearchKeywords.get_random(count=510)